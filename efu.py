#!/usr/bin/python3
import bz2
import codecs
import datetime
import os
import PythonMagick
import select
import re
import string
import subprocess
import sys
import time
import yaml

from datetime import datetime, timedelta
from optparse import OptionParser
from os       import path
from yaml     import safe_load

class EfuPythonLauncher(object):

  def __init__(
    self,
    default_cd_key                  = None,
    default_cd_key_filename         = None,
    default_chat_log_target_dir     = None,
    default_nwn_dir                 = None,
    default_nwn_executable_filename = None,
    default_screenshot_target_dir   = None
  ):
    self.chat_log_target_dir = default_chat_log_target_dir
    self.cd_key              = default_cd_key
    self.cd_key_filename     = default_cd_key_filename
    self.default_cd_key                  = \
      default_cd_key
    self.default_cd_key_filename         = \
      default_cd_key_filename
    self.default_chat_log_target_dir     = \
      default_chat_log_target_dir
    self.default_nwn_executable_filename = \
      default_nwn_executable_filename
    self.default_nwn_dir                 = \
      default_nwn_dir
    self.default_screenshot_target_dir   = \
      default_screenshot_target_dir
    self.nwn_arguments             = []
    self.nwn_dir                   = default_nwn_dir
    self.nwn_executable_filename   = default_nwn_executable_filename
    self.nwn_environment           = None
    self.screenshot_target_dir     = default_screenshot_target_dir
    self._date_prefix              = self._get_date_prefix()
    self._chat_log_archive_index   = None
    self._chat_log_target_suffix   = '.log.bz2'
    self._screenshot_archive_index = None
    self._screenshot_target_suffix = '.png'
    pass

  def _get_archive_index(self, directory, suffix):
    date_prefix = self.get_date_prefix()
    suffix_re   = re.sub('\.', '\.', suffix)
    filename_re = '^%s_([0-9]+)%s$' % (
      date_prefix, suffix_re
    )
    # highest_index_suffix = -1 so that we can add 1 to
    # highest_index_suffix and get 0 when target_filename is
    # assigned
    highest_index_suffix = -1
    for filename in os.listdir(directory):
      match = re.match(filename_re, filename)
      if match:
        index_suffix = int(match.group(1))
        if index_suffix > highest_index_suffix:
          highest_index_suffix = index_suffix

    return highest_index_suffix + 1

  def _get_chat_log_archive_index(self):
    if self._chat_log_archive_index is None:
      self._chat_log_archive_index = self._get_archive_index(
        self.chat_log_target_dir,
        self._chat_log_target_suffix
      )
    return self._chat_log_archive_index

  @staticmethod
  def _get_date_prefix():
    # Difference between Jan 1st 1970 and 12939 days 18000
    # I believe this represents real days since EFU
    # started.  When EFU started (2005?) epoch 0-time
    # (Jan 1st 1970) was probably 12939 days prior.
    days = (
      timedelta(days = 0,     seconds = time.time()) -
      timedelta(days = 12939, seconds = 18000      )
    ).days

    # days per 4 year (dp4y)
    dp4y   =  365 * 4 + 1
    # The (days - 1) below is a fix;  June 5th 2017
    # corresponded to Nightal 30th 1386 DR (the day before a new
    # four-year);  days - num_4y * dp4y was evaluating to 0, causing
    # this function to report the day as Hammer 0 1387 DR
    num_4y =  (days - 1) // dp4y
    # 1375 is the DR year that EFU started
    year  =  1375 + num_4y * 4
    days  =  days - num_4y * dp4y

    while True:
      # If it's a four year there are 366 days
      if year % 4 == 0 :
        if days < 367 :
          break
        else :
           days = days - 366
           year += 1
      else :
        if days < 366 :
          break
        else :
           days = days - 365
           year += 1
  
    if year % 4 > 0 :
      # days-per-month tuple
      dpm_tuple = (
        ('hammer'               , 31,             'midwinter'),
        ('alturiak'             , 30                         ),
        ('ches'                 , 30                         ),
        ('tarsakh'              , 31,            'greengrass'),
        ('mirtul'               , 30                         ),
        ('kythorn'              , 30                         ),
        ('flamerule'            , 31,             'midsummer'),
        ('eleasis'              , 30                         ),
        ('eleint'               , 31,       'high_harvestide'),
        ('marpenoth'            , 30                         ),
        ('uktar'                , 31, 'the_feast_of_the_moon'),
        ('nightal'              , 30                         )
      )
    else :
      dpm_tuple = (
        ('hammer'               , 31,             'midwinter'              ),
        ('alturiak'             , 30                                       ),
        ('ches'                 , 30                                       ),
        ('tarsakh'              , 31,            'greengrass'              ),
        ('mirtul'               , 30                                       ),
        ('kythorn'              , 30                                       ),
        ('flamerule'            , 32,             'midsummer', 'shieldmeet'),
        ('eleasis'              , 30                                       ),
        ('eleint'               , 31,       'high_harvestide'              ),
        ('marpenoth'            , 30                                       ),
        ('uktar'                , 31, 'the_feast_of_the_moon'              ),
        ('nightal'              , 30                                       )
      )
  
    for _dpm_tuple in dpm_tuple :
      if days <= _dpm_tuple[1] :
        if days < 31 :
          month_str = _dpm_tuple[0]
        elif days == 31 :
          month_str = _dpm_tuple[2]
        elif days == 32 :
          month_str = _dpm_tuple[3]
        break
      else :
        days = days - _dpm_tuple[1]
    
    return '%04d_%s_%02d' % (
      year, month_str, days
    ) 

  def _get_screenshot_archive_index(self):
    if self._screenshot_archive_index is None:
      self._screenshot_archive_index = self._get_archive_index(
        self.screenshot_target_dir,
        self._screenshot_target_suffix
      )
    return self._screenshot_archive_index

  def _incr_screenshot_archive_index(self):
    self._screenshot_archive_index += 1

  def archive_chat_log(self):
    chat_log_source_filename = os.path.join(
      self.nwn_dir, 'logs/nwclientLog1.txt'
    )
    if not os.path.isfile(chat_log_source_filename):
      self.warn(
        'No chat log file exists at %s;  No chat log will be archived' % (
          chat_log_source_filename
        )
      )
      return
    if not os.path.isdir(self.chat_log_target_dir):
      self.warn(
        'Chat log target directory did not exist at %s; creating it' % (
          self.chat_log_target_dir
        )
      )
      os.makedirs(self.chat_log_target_dir, mode=0o755)

    compressor = bz2.BZ2Compressor()

    # Necessary to avoid a UnicodeDecodeError
    #line_array = []
    with codecs.open(
      chat_log_source_filename, 
      "r+" ,
      encoding = 'utf-8',
      errors   = 'replace'
    ) as chat_log_source_file:
      for line in chat_log_source_file:
        match = re.match(r'^\[CHAT WINDOW TEXT\]\s+\[[^\]]+\]\s+(.*)', line)
        if match: 
          compressor.compress(bytes(match.group(1) + '\n', 'UTF-8'))
        else:
          compressor.compress(bytes(line, 'UTF-8'))
    chat_log_source_file.close()

    chat_log_target_file = open(
      os.path.join(
        self.chat_log_target_dir,
        '%s_%02d%s' % (
          self._date_prefix,
          self._get_chat_log_archive_index(),
          self._chat_log_target_suffix
        )
      ),
      'wb'
    )
    chat_log_target_file.write(
      compressor.flush()
    )
    chat_log_target_file.close()
    os.unlink(chat_log_source_filename)

  def archive_screenshots(self):
    source_filenames = list()
    source_path = os.path.join(self.nwn_dir, 'screenshots')

    # If the source screenshot directory doesn't exist, just
    # return
    if not os.path.exists(source_path):
      self.warn(
        "No screenshot directory exists at \"" + \
        source_path + "\"; No screenshots will be archived" 
      )
      return
    if not os.path.isdir(self.screenshot_target_dir):
      self.warn(
        'Screenshot target directory did not exist at %s; creating it' % (
          self.screenshot_target_dir
        )
      )
      os.makedirs(self.screenshot_target_dir, mode=0o755)

    for basename in os.listdir(source_path):
      filename = os.path.join(source_path, basename)
      if re.match(
        '^EFU_[0-9a-z]+\.tga$',
        basename
      ) and os.path.isfile(filename):
        source_filenames.append(filename)

    for filename in sorted(
      source_filenames,
      key = lambda filename: os.path.getmtime(filename)
    ):
      image = PythonMagick.Image(filename)
      image.quality(100)
      image.magick('PNG')
      image.write(
        os.path.join(
          self.screenshot_target_dir,
          '%s_%02d%s' % (
            self._date_prefix,
            self._get_screenshot_archive_index(),
            self._screenshot_target_suffix
          )
        )
      )
      self._incr_screenshot_archive_index()
      os.unlink(filename)

  def error(self, msg):
    print("Error: %s" % (msg), file = sys.stderr)
    sys.exit(1)

  def get_chat_log_target_dir(self):
    return self.chat_log_target_dir

  def get_date_prefix(self):
    return self._date_prefix

  def get_nwn_dir(self):
    return self.nwn_dir

  def get_screenshot_archive_index(self):
    date_prefix        = self.get_date_prefix()
    compression_suffix = self._chat_log_target_suffix
    filename_re        = '^%s_([0-9]+)\.log\.%s$' % (
      date_prefix, compression_suffix
    )
    # highest_count_suffix = -1 so that we can add 1 to
    # highest_count_suffix and get 0 when target_filename is
    # assigned
    highest_count_suffix = -1
    for filename in os.listdir(self.chat_log_target_dir):
      match = re.match(filename_re, filename)
      if match:
        count_suffix = int(match.group(1))
        if count_suffix > highest_count_suffix:
          highest_count_suffix = count_suffix

    target_filename = "%s_%02d.log.%s" % (
      date_prefix,
      highest_count_suffix + 1,
      compression_suffix
    )
    return target_filename


  def get_screenshot_target_dir(self):
    return self.screenshot_target_dir

  def launch_nwn(self):
    if not os.path.isfile(self.nwn_executable_filename):
      self.warn(
        'No nwn executable exists at %s; Exiting' % (
          self.nwn_executable_filename
        )
      )
      sys.exit(1)

    # make a merged copy of the OS environment and any
    # nwn_environment settings specified
    _environment = os.environ.copy()
    if self.nwn_environment:
      _environment.update(self.nwn_environment)

    # Store the current working directory
    prev_cwd = os.getcwd() 
    # Change the current working directory to be the same as
    # that of the executable;  This is necessary for the
    # Beamdog client.
    os.chdir(
      os.path.dirname(
        self.nwn_executable_filename
      )
    )

    subproc = subprocess.Popen(
      [self.nwn_executable_filename] + self.nwn_arguments,
      env = _environment
    )
    subproc.wait()
    os.chdir(prev_cwd)

  def warn(self, msg):
    print("Warning: %s" % (msg), file = sys.stderr)

  def write_cd_key(self):
    if self.cd_key is None:
      return

    if os.path.exists(self.cd_key_filename):
      if os.path.isdir(self.cd_key_filename):
        self.error(
          "cd_key_filename " + self.cd_key_filename + "is " + \
          "a directory"
        )
      if os.path.isfile(self.cd_key_filename):
        self.warn(
          "cd_key_filename " + self.cd_key_filename + " " + \
          "exists; Overwriting it"
        )

    cd_key_file = open(self.cd_key_filename, 'w+')
    cd_key_file.write(
      ";This is your CD Key for Neverwinter Nights.\n"    + \
      ";DO NOT share this CD Key with ANYONE.\n"          + \
      ";Apart from this installation, Beamdog, BioWare, " + \
      "and Atari will never ask you for this CD Key.\n"   + \
      "[NWN1]\n"                                          + \
      "YourKey=" + self.cd_key + "\n"
    )

class EfuPythonLauncherYaml(EfuPythonLauncher):

  def __init__(
    self,
    filename,
    default_cd_key                  = None,
    default_cd_key_filename         = None,
    default_chat_log_target_dir     = None,
    default_nwn_dir                 = None,
    default_nwn_executable_filename = None,
    default_screenshot_target_dir   = None
  ):
    EfuPythonLauncher.__init__(
      self,
      default_cd_key                  = default_cd_key,
      default_cd_key_filename         = default_cd_key_filename,
      default_chat_log_target_dir     = default_chat_log_target_dir,
      default_nwn_executable_filename = default_nwn_executable_filename,
      default_nwn_dir                 = default_nwn_dir,
      default_screenshot_target_dir   = default_screenshot_target_dir
    )
    self.nwn_ini       = dict()
    self.nwnplayer_ini = dict()

    yaml_dict = self.yaml_load(filename)
    for key, value in yaml_dict.items():
      setattr(
        self,
        key,
        value
      )
    self.merge_nwn_ini()
    self.merge_nwnplayer_ini()

  def merge_nwn_ini(self):
    nwn_ini_filename = os.path.join(
      self.nwn_dir,
      'nwn.ini'
    )
    if not os.path.isfile(nwn_ini_filename):
      self.warn(nwn_ini_filename + " not found; " + \
        "will not be made"
      )
      return
    self.merge_ini_dict(
      self.nwn_ini,
      nwn_ini_filename
    )

  def merge_nwnplayer_ini(self):
    nwnplayer_ini_filename = os.path.join(
      self.nwn_dir,
      'nwnplayer.ini'
    )
    if not os.path.isfile(nwnplayer_ini_filename):
      self.warn(nwnplayer_ini_filename + " not found; " + \
        "will not be made"
      )
      return
    self.merge_ini_dict(
      self.nwnplayer_ini,
      nwnplayer_ini_filename
    )

  @staticmethod
  def merge_ini_dict(ini_dict, filename):
    ini_file = open(filename, 'r')
    section_rank = 0
    section = None
    comment_list = list()
    for line in ini_file:
      match = re.match('^\[([^\]]+)\]\s*$', line)
      if match:
        section = match.group(1)
        if not section in ini_dict:
          ini_dict[section] = dict()
        if not 'settings' in ini_dict[section]:
          ini_dict[section]['settings'] = dict()
        setting_dict = ini_dict[section]['settings']
        if not 'rank' in ini_dict[section]:
          ini_dict[section]['rank'] = section_rank
          section_rank = section_rank + 1
        setting_rank = 0
        if not 'comment' in ini_dict[section]:
          ini_dict[section]['comment'] = comment_list
        comment_list = list()
        next
      match = re.match('^([^=]+)\s*=\s*(.*)$', line)
      if match:
        if section is None:
          error("Poorly formed ini file " + filename)
        setting = match.group(1)
        if not setting in setting_dict:
          setting_dict[setting] = dict()
        if not 'value' in setting_dict[setting]:
          setting_dict[setting]['value'] = match.group(2)
        if not 'rank' in setting_dict[setting]:
          setting_dict[setting]['rank']  = setting_rank
          setting_rank = setting_rank + 1
        if not 'comment' in setting_dict[setting]:
          setting_dict[setting]['comment'] = comment_list
        comment_list = list()        
        next
      match = re.match('^([#;].*)', line)
      if match:
        comment_list.append(match.group(1))
        next
    ini_file.close()

  @staticmethod
  def write_ini(ini_dict, filename):
    if not os.access(filename, os.W_OK):
      self.warn(filename + " not writable; " + \
        "changes will not be made"
      )
      return
    out_file = open(filename, 'w+')
    for section, section_dict in sorted(
      ini_dict.items(),
      key = lambda kv: kv[1]['rank']
    ):
      if len(section_dict['comment']) > 0:
        out_file.write(
          "\n".join(section_dict['comment']) + "\n"
        )
      out_file.write('[' + section + ']\n')
      for setting, setting_dict in sorted(
        section_dict['settings'].items(),
        key = lambda kv: kv[1]['rank']
      ):
        if len(setting_dict['comment']) > 0:
          out_file.write(
            "\n".join(setting_dict['comment']) + "\n"
          )
        out_file.write(
          setting + "=" + str(setting_dict['value']) + "\n"
        )

  def write_nwn_ini(self):
    if len(self.nwn_ini) < 1:
      return
    nwn_ini_filename = os.path.join(
      self.nwn_dir,
      'nwn.ini'
    )
    self.write_ini(self.nwn_ini, nwn_ini_filename)

  def write_nwnplayer_ini(self):
    if len(self.nwnplayer_ini) < 1:
      return
    nwnplayer_ini_filename = os.path.join(
      self.nwn_dir,
      'nwnplayer.ini'
    )
    self.write_ini(self.nwnplayer_ini, nwnplayer_ini_filename)

  @staticmethod
  def yaml_load(filename):
    yaml_file = open(filename, 'r+')
    yaml_dict = yaml.safe_load(yaml_file)
    yaml_file.close()
    return yaml_dict
 
class EfuPythonLauncherCmd(EfuPythonLauncherYaml):

  def __init__(
    self,
    argv,
  ):
    default_cd_key = None
    default_cd_key_filename = os.path.join(
      os.environ['HOME'], '.local/share/Neverwinter Nights/cdkey.ini'
    )
    default_chat_log_target_dir = os.path.join(
      os.environ['HOME'], 'efu/chat_logs'
    )
    default_nwn_executable_filename = os.path.join(
      os.environ['HOME'], '.local/share/Steam/steamapps/common/Neverwinter Nights/bin/linux-x86/nwmain-linux'
    )
    default_nwn_dir = os.path.join(
      os.environ['HOME'], '.local/share/Neverwinter Nights'
    )
    default_screenshot_target_dir = os.path.join(
      os.environ['HOME'], 'efu/screenshots'
    )
    option_parser = self.configure_parser(
      argv,
      default_cd_key                  = default_cd_key,
      default_cd_key_filename         = default_cd_key_filename,
      default_chat_log_target_dir     = default_chat_log_target_dir,
      default_nwn_executable_filename = default_nwn_executable_filename,
      default_nwn_dir                 = default_nwn_dir,
      default_screenshot_target_dir   = default_screenshot_target_dir
    )
    (options, args) = option_parser.parse_args()
    EfuPythonLauncherYaml.__init__(
      self,
      options.config_file,
      default_chat_log_target_dir     = default_chat_log_target_dir,
      default_nwn_executable_filename = default_nwn_executable_filename,
      default_nwn_dir                 = default_nwn_dir,
      default_screenshot_target_dir   = default_screenshot_target_dir
    )
    for key, value in options.__dict__.items():
      if value == None:
        pass
      elif isinstance(value, (list, tuple)):
        setattr(
          self,
          key,
          [obj(x) if isinstance(x, dict) else x for x in value]
        )
      else:
        setattr(
          self,
          key,
          obj(value) if isinstance(value, dict) else value
        )
    if len(args) > 0:
      self.nwn_arguments = args

    if not os.path.isdir(self.chat_log_target_dir):
      os.makedirs(self.chat_log_target_dir)

    if not os.path.isdir(self.screenshot_target_dir):
      os.makedirs(self.screenshot_target_dir)

  @staticmethod
  def configure_parser(
    argv,
    default_cd_key,
    default_cd_key_filename,
    default_chat_log_target_dir,
    default_nwn_executable_filename,
    default_nwn_dir,
    default_screenshot_target_dir
  ):
  
    parser = OptionParser(
      usage   = 'Usage: %prog [options] [NWN_ARGUMENT ...]',
      version = '%prog 1.0'
    )
  
    parser.set_defaults(
      config_file = os.path.join(
        os.path.dirname(argv[0]), 'config_file'
      )
    )
    parser.add_option(
      "-c",
      "--config",
      action = "store",
      type   = "string",
      dest   = "config_file",
      help   = "The configuration .yaml file"
    )

    parser.set_defaults(chat_log_target_dir = None)
    parser.add_option(
      "-C",
      "--chat-log-target-dir",
      action = "store",
      type   = "string",
      dest   = "chat_log_target_dir",
      help   = "The directory to move chat_log files to;  "
        "Default: " + default_chat_log_target_dir
    )

    parser.set_defaults(nwn_executable_filename = None)
    parser.add_option(
      "-e",
      "--nwn-executable-filename",
      action = "store",
      type   = "string",
      dest   = "nwn_executable_filename",
      help   = "The full filename of the nwn executable to "
        "run;  Default: " + default_nwn_executable_filename
    )

    parser.set_defaults(cd_key = default_cd_key)
    parser.add_option(
      "-k",
      "--cd-key",
      action = "store",
      type   = "string",
      dest   = "cd_key",
      help   = "The NWN CD key to use. Form: "
        "XXXXX-XXXXX-XXXXX-XXXXX-XXXXX-XXXXX-XXXXX;  "
        "Default: "
        "None" if default_cd_key is None else default_cd_key
    )

    parser.set_defaults(
      cd_key_filename = default_cd_key_filename
    )
    parser.add_option(
      "-K",
      "--cd-key-filename",
      action = "store",
      type   = "string",
      dest   = "cd_key_filename",
      help   = "The full filename of the cdkey.ini file "
        "that the executable reads;  This is so that the "
        "key can be changed by the --cd-key option.  "
        "Default: " + default_cd_key_filename
    )

    parser.set_defaults(nwn_dir = None)
    parser.add_option(
      "-n",
      "--nwn-dir",
      action = "store",
      type   = "string",
      dest   = "nwn_dir",
      help   = "The directory home of the Neverwinter "
        "Nights files;  Default: " + default_nwn_dir
    )

    parser.set_defaults(screenshot_target_dir = None)
    parser.add_option(
      "-S",
      "--screenshot-target-dir",
      action = "store",
      type   = "string",
      dest   = "screenshot_target_dir",
      help   = "The directory to move screenshot files "
        "to;  Default: " + default_screenshot_target_dir
    )
 
    return parser
 
def main():
  efu_python_launcher = EfuPythonLauncherCmd(sys.argv)
  efu_python_launcher.write_cd_key()
  efu_python_launcher.write_nwn_ini()
  efu_python_launcher.write_nwnplayer_ini()
  efu_python_launcher.launch_nwn()
  efu_python_launcher.archive_chat_log()
  efu_python_launcher.archive_screenshots()
  sys.exit(0)

if __name__ == '__main__':
  main()
